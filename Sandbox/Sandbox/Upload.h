#pragma once

#include <vector>
#include <string>

#include "Model.h"

class Upload
{
public:
	Upload(std::string path);
	~Upload();

	void uploadObjects(Shader &ourShader ,  glm::mat4& model);

	std::string strExePath;
	std::vector<Model> modelVector;
};