#include "Upload.h"



Upload::Upload(std::string path):
	strExePath(path)
{
	modelVector = 
	{
		Model(strExePath + "\\Grass\\10450_Rectangular_Grass_Patch_v1_iterations-2.obj"),
		Model(strExePath + "\\Medieval House\\medieval house.obj"),
		Model(strExePath + "\\Farm House\\farmhouse_obj.obj"),
		Model(strExePath + "\\Casa\\casa.obj"),
		Model(strExePath + "\\Snowy House\\Snow covered CottageOBJ.obj"),
		Model(strExePath + "\\Medieval Building\\MedievalBilding.obj"),
		Model(strExePath + "\\Castle\\Castle OBJ.obj"),
		Model(strExePath + "\\Tower\\wooden watch tower2.obj"),
		Model(strExePath + "\\Barrel\\MedievalBarrel_OBJ.obj"),
		Model(strExePath + "\\Dog v1\\12228_Dog_v1_L2.obj"),
		Model(strExePath + "\\Bird\\12214_Bird_v1max_l3.obj"),
		Model(strExePath + "\\Cat\\12221_Cat_v1_l3.obj"),
		Model(strExePath + "\\Christmas Tree\\12150_Christmas_Tree_V2_L2.obj"),
	};

}


Upload::~Upload()
{
}

void Upload::uploadObjects(Shader& ourShader , glm::mat4& model)
{
	//0 grass
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-5.0f, -4.0f, -5.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	ourShader.setMat4("model", model);
	modelVector[0].Draw(ourShader);

	//1 medieval house

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-35.0f, 0.0f, -15.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-35.0f, 1.0f, 5.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-35.0f, 1.4f, 30.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-40.0f, 0.7f, 40.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(15.0f, -1.7f, 40.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(7.0f, -0.5f, 20.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-4.0f, -0.2f, 40.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-4.0f, 0.2f, -32.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(0.0f, 0.8f, -50.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(0.0f, 0.2f, -4.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-5.0f, 0.2f, -15.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(15.0f, 1.1f, -15.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(15.0f, 1.4f, -23.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(24.0f, 1.4f, -23.0f));
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
	ourShader.setMat4("model", model);
	modelVector[1].Draw(ourShader);



	//2 farm house
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(0.0f, -0.1f, 4.0f));
	model = glm::rotate(model, (float)glm::radians(5.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-4.0f, 0.0f, -4.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(5.0f, 0.0f, -35.0f));
	model = glm::rotate(model, (float)glm::radians(2.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(0.0f, -0.48f, 20.0f));
	model = glm::rotate(model, (float)glm::radians(2.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(0.0f, 0.2f, -20.0f));
	model = glm::rotate(model, (float)glm::radians(2.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(8.0f, 1.0f, -20.0f));
	model = glm::rotate(model, (float)glm::radians(2.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(10.0f, -1.7f, 34.0f));
	model = glm::rotate(model, (float)glm::radians(5.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-10.0f, 0.0f, -30.0f));
	model = glm::rotate(model, (float)glm::radians(5.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-20.0f, 1.5f, -65.0f));
	model = glm::rotate(model, (float)glm::radians(5.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-34.0f, 0.7f, -64.0f));
	model = glm::rotate(model, (float)glm::radians(5.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
	ourShader.setMat4("model", model);
	modelVector[2].Draw(ourShader);

	//3 casa

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-23.0f, 0.0f, 10.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-42.0f, 1.6f, 10.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(26.0f, -0.9f, 38.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(30.0f, 0.7f, 22.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-55.0f, 1.0f, 20.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(13.0f, -0.2f, 20.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(30.0f, 0.7f, 53.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(12.0f, 0.7f, 56.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);



	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-12.0f, 0.0f, 40.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-12.0f, 0.0f, 25.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-5.0f, 0.0f, 30.0f));
	model = glm::rotate(model, (float)glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(4.0f, 0.2f, -4.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);



	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(22.0f, 0.4f, -33.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-10.0f, 0.2f, -41.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(5.0f, 0.6f, -45.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(15.0f, 0.2f, -41.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-20.0f, 0.1f, -38.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[3].Draw(ourShader);



	//4 snowy house
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(4.0f, -0.2f, 4.0f));
	model = glm::rotate(model, (float)glm::radians(5.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[4].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(2.0f, -0.7f, 12.0f));
	model = glm::rotate(model, (float)glm::radians(5.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[4].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(33.0f, 1.0f, -30.0f));
	model = glm::rotate(model, (float)glm::radians(5.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[4].Draw(ourShader);

	//5 medieval building 


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(15.0f, 0.7f, -47.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(15.0f, -0.2f, -34.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);



	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-5.0f, -0.2f, -40.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-20.0f, -0.2f, -30.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);



	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-20.0f, -0.2f, 30.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-20.0f, -0.2f, 20.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-35.0f, 1.1f, 20.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-34.0f, 1.1f, 12.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-40.0f, 0.9f, 4.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-4.0f, 0.1f, 4.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-25.0f, -0.7f, -10.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(25.0f, 0.8f, 14.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(23.0f, -0.5f, 25.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(16.0f, -0.5f, 25.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(35.0f, 0.9f, 22.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(35.0f, 0.2f, 35.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));
	ourShader.setMat4("model", model);
	modelVector[5].Draw(ourShader);



	//6 castle
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(20.0f, -0.6f, 0.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.3f, 0.3f, 0.3f));
	ourShader.setMat4("model", model);
	modelVector[6].Draw(ourShader);

	//7 tower
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-10.0f, -1.0f, -4.0f));
	model = glm::scale(model, glm::vec3(0.7f, 0.7f, 0.7f));
	ourShader.setMat4("model", model);
	modelVector[7].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(45.0f, 0.5f, -6.0f));
	model = glm::scale(model, glm::vec3(0.7f, 0.7f, 0.7f));
	ourShader.setMat4("model", model);
	modelVector[7].Draw(ourShader);

	//7 tower
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-10.0f, -0.8f, 4.0f));
	model = glm::scale(model, glm::vec3(0.7f, 0.7f, 0.7f));
	ourShader.setMat4("model", model);
	modelVector[7].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(45.0f, 0.5f, 2.0f));
	model = glm::scale(model, glm::vec3(0.7f, 0.7f, 0.7f));
	ourShader.setMat4("model", model);
	modelVector[7].Draw(ourShader);

	//8 barrelssss
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(2.0f, -0.05f, 3.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[8].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(2.2f, -0.05f, 3.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[8].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(2.0f, -0.05f, 2.8f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[8].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(2.0f, 0.2f, -3.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[8].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-2.0f, 0.12f, -3.0f));
	model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
	ourShader.setMat4("model", model);
	modelVector[8].Draw(ourShader);

	//9 dogo v1

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-10.0f, -0.3f, 0.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[9].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(0.9f, 0.12f, 2.9f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[9].Draw(ourShader);

	//10 birdy
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-2.0f, 0.35f, -3.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[10].Draw(ourShader);

	//11 cat
	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-3.0f, 0.12f, 2.9f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, (float)glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[11].Draw(ourShader);

	//12 christmas tree

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(5.0f, -0.3f, -29.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(0.0f, -0.3f, -25.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-12.0f, -0.3f, -20.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);



	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-18.0f, -0.3f, 5.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-12.0f, -0.3f, 0.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);



	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(3.0f, -0.3f, 24.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(14.0f, -0.3f, 14.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(36.0f, 0.4f, 20.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(39.0f, 0.4f, -16.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(39.0f, 0.4f, -5.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(37.0f, 0.4f, 2.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(37.0f, 0.4f, 14.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(30.0f, 0.4f, 14.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(18.0f, -0.3f, 24.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(37.0f, 0.2f, 33.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);


	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(1.8f, 0.0f, -4.5f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(1.8f, 0.0f, -10.5f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-3.0f, 0.0f, -10.5f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-6.0f, 0.0f, -10.5f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(-12.0f, 0.0f, -10.5f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(20.0f, 0.5f, -13.5f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(15.0f, 0.5f, -13.5f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(13.0f, 0.5f, -10.5f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);

	model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(13.0f, 0.5f, -20.0f));
	model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
	ourShader.setMat4("model", model);
	modelVector[12].Draw(ourShader);
}
